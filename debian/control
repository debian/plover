Source: plover
Section: utils
Priority: optional
Maintainer: Harlan Lieberman-Berg <hlieberman@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-exec,
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               pyqt5-dev-tools,
               python3,
               python3-babel,
               python3-dbus,
               python3-evdev,
               python3-hid,
               python3-plover-stroke,
               python3-pkg-resources,
               python3-platformdirs,
               python3-pyqt5,
               python3-pytest <!nocheck>,
               python3-pytest-xvfb <!nocheck>,
               python3-pytestqt <!nocheck>,
               python3-rtf-tokenize,
               python3-serial (>= 2.7),
               python3-setuptools,
               python3-setuptools-scm,
               python3-wcwidth (>= 0.1.7),
               python3-xlib (>= 0.16)
Standards-Version: 4.7.0
Homepage: https://www.openstenoproject.org/plover/
Vcs-Git: https://salsa.debian.org/debian/plover.git
Vcs-Browser: https://salsa.debian.org/debian/plover
Rules-Requires-Root: no

Package: plover
Architecture: all
Depends: python3-dbus,
         python3-evdev,
         python3-hid,
         python3-pyqt5,
         python3-xlib (>= 0.16),
         ${misc:Depends},
         ${python3:Depends}
Description: free stenography engine and typing tool
 Plover is a small, slick Python application that runs in the
 background and acts as a translator to read steno movements and
 emulate keystrokes in a way that the programs you are using can't
 even tell you're using steno.
 .
 Plover is used today by hundreds of people, ranging from professional
 stenographers, transcriptionists, programmers, and hobbyists.  It
 supports:
 .
  - Regular keyboards and real steno machines
  - Media key support to control media, change volume, etc.
  - Comes with a well-developed theory extended from StenEd,
    but with over 50,000 new strokes.
  - Supports JSON and RTF/CRE dictionaries
  - Includes training tools to help you learn
